#include "singletons.h"
//#include "AudioManager.h"

#include <algorithm>
#include <sstream>

AudioManager* AudioManager::pInstance = NULL;

AudioManager* AudioManager::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new AudioManager();
	}
	return pInstance;
}

AudioManager::AudioManager(){
	mFirstFreeSlot = 0;
}

AudioManager::~AudioManager(){
	mAudiosMap.clear();
	mAudiosVector.clear();
	mIDMap.clear();
}

int AudioManager::addAudio(const char* file){
	//Carga una imagen y la guarda en los contenedores
	ofSoundPlayer* audio1 = new ofSoundPlayer();

	bool loaded = audio1->load(file);

    if (!loaded){
		std::cerr << "Error when loading audio: "<< file << std::endl;
		return -1;
    }
    else {
		mAudiosMap.insert(std::pair<std::string, ofSoundPlayer*>(file, audio1));
		int returnValue = 0;
		if(mFirstFreeSlot == mAudiosVector.size()){
			mAudiosVector.push_back(audio1);
			mFirstFreeSlot++;
			returnValue = mAudiosVector.size()-1;
		}else{
			mAudiosVector[mFirstFreeSlot] = audio1;
			int temp = mFirstFreeSlot;
			mFirstFreeSlot = updateFirstFreeSlot();
			returnValue = temp;
		}
		return returnValue;
    }
}

ofSoundPlayer* AudioManager::getAudio(const char* file){
	// Si la imagen ha sido cargada, devuelve el puntero
	// Si no, carga la imagen y devuelve el puntero

	std::map<std::string, ofSoundPlayer*>::iterator it = mAudiosMap.find(file);
	if(it == mAudiosMap.end()){
		std::cout<<"Adding Audio " << file << " (first request)"<<std::endl;
		addAudio(file);
		it = mAudiosMap.find(file);
	}
	return it->second;
}

int AudioManager::getAudioID(const char* file){
	// Mira si el gráfico ya ha sido cargado
	// Si no ha sido cargado, lo carga y devuelve la última posición del vector
	// Si ya este en el map, busca el gráfico en el vector y devuelve la posición

	std::map<std::string, ofSoundPlayer*>::iterator it = mAudiosMap.find(file);
	int returnValue = -1;
	if(it == mAudiosMap.end()){
		std::cout<<"Adding Audio " << file << " (first request)"<<std::endl;
		int index = addAudio(file);
		mIDMap.insert(std::pair<std::string, int>(file, index));
		returnValue = index;
	}else{
		std::map<std::string,int>::iterator iter = mIDMap.find(file);
		if(iter == mIDMap.end()){
			std::cout<<"Failed to find "<<file<<" on the ID map"<<std::endl;
			returnValue = searchAudio(it->second);
		}else{
			returnValue = iter->second;
		}
	}

	return returnValue;
}

ofSoundPlayer* AudioManager::getAudioByID(int ID){
	// Si el ID que se le pide est� dentro del rango del vector,
	// devuelve el valor en esa posici�n
	// Una ID nunca sera <0 porque size es sin signo. Aqui GCC daba un warning.
	if (ID < mAudiosVector.size()){
		return mAudiosVector[ID];
	}else{
		return NULL;
	}
}

int AudioManager::searchAudio(ofSoundPlayer* sound){
	// Busca el gr�fico en el vector, si no lo encuentra, devuelve -1

	for(int i = 0; i < mAudiosVector.size(); i++){
		if(mAudiosVector[i] == sound){
			return i;
		}
	}
	return -1;
}

std::string AudioManager::getAudioPathByID(int ID){
	if(ID >= mAudiosVector.size()){return "NULL";}
	ofSoundPlayer* aSurface = mAudiosVector[ID];

	std::map<std::string, ofSoundPlayer*>::iterator it;
	for(it = mAudiosMap.begin(); it != mAudiosMap.end(); ++it){
		if (it->second == aSurface){
			return it->first;
		}
	}
	return "NULL";
}


int AudioManager::updateFirstFreeSlot(){
	for(int i = 0; i < mAudiosVector.size(); i++){
		if(mAudiosVector[i] == NULL){
			return i;
		}
	}
	return mAudiosVector.size();
}

void AudioManager::printLoadedAudios(){
	std::map<std::string, ofSoundPlayer*>::iterator it = mAudiosMap.begin();
	std::cout<<"---- Loaded Graphics ----"<<std::endl;
	while(it != mAudiosMap.end()){
		std::cout<< it->first << std::endl;
		it++;
	}
	std::cout<<"-------------------------"<<std::endl;
}