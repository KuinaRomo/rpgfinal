#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include "includes.h"
#include "ofMain.h"
#include <map>
#include <vector>

//! ResourceManager class
/*!
	Handles the load and management of the graphics in the game.
*/
class AudioManager
{
	public:
		//! Constructor of an empty ResourceManager.
		AudioManager();

		//! Destructor.
		~AudioManager();

		//! Gets the graphic ID
		/*!
			\param file Filepath to the graphic
			\return ID of the graphic
		*/
		int getAudioID(const char* file);

		//! Gets the graphic path given an ID
		/*!
			\param ID of the graphic
			\return Filepath to the graphic
		*/
		std::string getAudioPathByID(int ID);

		//! Returns the ofImage of the graphic
		/*!
			\param ID ID of the graphic
			\return ofImage
		*/
		ofSoundPlayer* getAudioByID(int ID);

		//! Prints the path to loaded graphics
		void printLoadedAudios();

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "AudioManager";};

		//! Gets Singleton instance
		/*!
			\return Instance of ResourceManager (Singleton).
		*/
		static AudioManager* getInstance();

	private:

		//! Adds a graphic to the ResourceManager
		/*!
			\param file Filepath to the graphic
			\return -1 if there's an error when loading
		*/
		int addAudio(const char* file);

		//! Searches in the ResourceManager and gets the graphic by its name. If it isn't there, loads it
		/*!
			\param file Filepath of the graphic
			\return ofImage
		*/
		ofSoundPlayer* getAudio(const char* file);

		//! Searches the graphic in the vector and returns its ID
		/*!
			\param img ofImage of the graphic
			\return ID of the graphic
		*/
		int searchAudio(ofSoundPlayer* song);

		//! Searches the first NULL in mGraphicsVector and updates mFirstFreeSlot to store its position
		/*!
			\return Index of the first NULL in mGraphicsVector
		*/
		int updateFirstFreeSlot();

		std::map<std::string, ofSoundPlayer*> 	mAudiosMap;	/*!<  Map that stores Surfaces. Useful for direct access*/
		std::vector<ofSoundPlayer*>				mAudiosVector;/*!<  Vector that stores Surfaces. Useful for sequential access*/
		std::map<std::string, int>				mIDMap;			/*!<  Map that stores ID. Links strings to ID, useful for sequential access*/

		int										mFirstFreeSlot;	/*!<  First free slot in the mGraphicsVector*/
		static AudioManager*					pInstance;		/*!<  Singleton instance*/
};

#endif
