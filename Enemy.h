#ifndef ENEMY_H
#define ENEMY_H

#include "includes.h"

class Enemy
{
	public:
		Enemy();
		~Enemy();

		void update();
		void render();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Template";};

	protected:
	
	private:
	
};

#endif
