//Include our classes
#include "FightEntity.h"

FightEntity::FightEntity(){
	mAttack = false;
	mPrepared = false;
	mLoading = 0;
	mTotalLoad = 250;

	font.loadFont("final_fantasy_36_font.ttf", 20);
}

FightEntity::~FightEntity(){
}

void FightEntity::update(){
	if (!mAttack && mLoading < mTotalLoad) {
		mLoading += mLoadVelocity;
		if (mLoading >= mTotalLoad) {
			mPrepared = true;
			mAttack = true;
		}
	}
}
void FightEntity::render(){
	
}

bool FightEntity::isOfClass(std::string classType){
	if(classType == "FightEntity"){
		return true;
	}
	return false;
}

int FightEntity::getLife() {
	return mLife;
}

void FightEntity::setLife(int life) {
	mLife = life;
}

void FightEntity::setLoading(float load) {
	mLoading = load;
	return;
}

float FightEntity::getLoading() {
	return mLoading;
}

void FightEntity::setAttack(bool att) {
	mAttack = att;
	return;
}

bool FightEntity::getAttack() {
	return mAttack;
}

std::string FightEntity::getName() {
	return mName;
}

void FightEntity::setPrepared(bool prep) {
	mPrepared = prep;
	return;
}

bool FightEntity::getPrepared() {
	return mPrepared;
}

