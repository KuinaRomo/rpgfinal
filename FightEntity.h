#ifndef FIGHTENTITY_H
#define FIGHTENTITY_H

#include "includes.h"
#include "ofMain.h"
#include "includes.h"
#include "Renderer.h"
#include "Utils.h"
#include "ofmain.h"
#include "singletons.h"


class FightEntity
{
	public:
		FightEntity();
		~FightEntity();

		virtual void update();
		virtual void render();

		bool isOfClass(std::string classType);
		virtual std::string getClassName(){return "FightEntity";};

		float getLoading();
		void setLoading(float load);

		void setAttack(bool att);
		bool getAttack();
		std::string getName();

		int getLife();
		void setLife(int life);

		void setPrepared(bool prep);
		bool getPrepared();

	protected:
		//!La vida de las EntityFight
		int mLife;
		//!Es el width total de la barra de carga
		int mTotalLoad;
		//!Numero que se incrementa a mLoading en cada ronda del bucle siempre que sea menor que mTotalLoad
		float mLoadVelocity;
		//!El width de la barra de carga que se va incrementando haciendo la barra de carga al usarlo en el render como width
		float mLoading;

		//!Se pone a true cuando la barra de carga se llena 
		bool mAttack;
		//!Se pone a true cuando la barra de carga se llena y a false cuando la entity ha entrado en el vector mEntities
		bool mPrepared;
		//!Nombre de la entidad
		std::string mName;
		//!Fuente usada para poner información sobre la lucha
		ofTrueTypeFont font;
		
	private:
	
};

#endif
