//Include our classes
#include "MapManager.h"
#include "Renderer.h"
#include "singletons.h"

#define byte char
#define Uint16 unsigned short int
#define Sint16 short int
#define Uint32 unsigned int
#define Sint32 int

MapManager* MapManager::pInstance = NULL;

MapManager* MapManager::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new MapManager();
	}
	return pInstance;
}

MapManager::MapManager(){
	mpMapWidth = 0;
	mpMapHeight = 0;
	mpLayers = 0;
	mpObjectLayer = 1;
	mpMapLoaded = false;
	mpTilesetID = -1;
	mpTilesetRect = C_Rectangle{ 0,0,TILE_SIZE, TILE_SIZE };
}

MapManager::~MapManager(){
}

void MapManager::loadMap(const char* file_path){ //DO NOT TOUCH UNDER DANGER OF SETTING YOUR CROTCH ON FIRE
	//READING FOR BETTER UNDERSTANDING ON HOW TO READ BINARY FILES IS OK, THOUGH.
	std::cout << "Loading Map file " << file_path << "." << std::endl;
	std::fstream file;
	file.open(file_path, std::ios::in | std::ios::binary);
	if (!file.is_open()) {
		std::cout << "ERROR opening file " << file_path << ". Is it there?" << std::endl;
		system("pause");
		exit(0);
		return;
	}
	//Read Unused bytes
		//Orientation
		byte orientation;
		file.read((byte*)&orientation, sizeof(byte));
		// Stagger axis
		byte stagger_axis;
		file.read((byte*)&stagger_axis, sizeof(byte));
		// Stagger index
		byte stagger_index;
		file.read((byte*)&stagger_index, sizeof(byte));
		// Hex side length
		Uint16 hex_side_length;
		file.read((byte*)&hex_side_length, sizeof(Uint16));
	//Map Width and Height
	file.read((byte*)&mpMapWidth, sizeof(Uint16));
	file.read((byte*)&mpMapHeight, sizeof(Uint16));
	std::cout << ">> Map size: " << mpMapWidth << " x " << mpMapHeight << std::endl;

	//Tile Size
	Uint16 tile_size_w, tile_size_h;
	file.read((byte*)&tile_size_w, sizeof(Uint16));
	file.read((byte*)&tile_size_h, sizeof(Uint16));
	if (tile_size_h != tile_size_w || tile_size_h != TILE_SIZE) {
		std::cout << "ERROR tile size is not " << TILE_SIZE << "." << std::endl;
		system("pause");
		exit(0);
		return;
	}

	// Tile bits (16-bit only currently)
	byte tile_bytes;
	file.read((byte*)&tile_bytes, sizeof(byte));

	// Run length encoding (0=false, 24=24bit, 32=32bit)
	byte rle;
	file.read((byte*)&rle, sizeof(byte));

	if (rle != 0) {
		std::cout << "ERROR compression not supported. Please disable Run Length Encoding in the converter"<< std::endl;
		system("pause");
		exit(0);
		return;
	}

	//Layers
	file.read((byte*)&mpLayers, sizeof(Uint16));
	std::cout << ">> Number of layers: " << mpLayers << std::endl;
	if (mpLayers < 2) {
		std::cout << "ERROR. There must be more than a collision layer" << std::endl;
		system("pause");
		exit(0);
		return;
	}

	//Resize Maps map
	mpLayers--;
	mpMapLayers.resize(mpLayers);
	mpCollisionLayer.resize(mpMapHeight);
	for (int i = 0; i < mpMapHeight; i++) {
		mpCollisionLayer[i].resize(mpMapWidth);
	}
	for (int l = 0; l < mpLayers; l++) {
		mpMapLayers[l].resize(mpMapHeight);
		for (int i = 0; i < mpMapHeight; i++) {
			mpMapLayers[l][i].resize(mpMapWidth);
		}
	}

	Uint32 layer_size = mpMapHeight * mpMapWidth;
	Uint32 pos = 0, xPos = 0, yPos = 0;
	Uint16 value = 0;
	//Read Collision Map
	while (pos < layer_size) {
		bool collision = false;
		file.read((byte*)&value, sizeof(Uint16));
		if (value > 1) {
			collision = true;
		}
		mpCollisionLayer[yPos][xPos] = collision;
		xPos++;
		if (xPos >= mpMapWidth) {
			xPos = 0;
			yPos++;
		}
		pos++;
	}
	//Read Graphic Layers
	for(int l = 0; l < mpLayers; l++) {
		pos = 0;
		xPos = 0;
		yPos = 0;
		while (pos < layer_size) {
			file.read((byte*)&value, sizeof(Uint16));
			if (value > 0) {
				value--;
			}
			mpMapLayers[l][yPos][xPos] = value;
			xPos++;
			if (xPos >= mpMapWidth) {
				xPos = 0;
				yPos++;
			}
			pos++;
		}
	}
	std::cout << "Map read successfully" << std::endl;
	mpMapLoaded = true;
	mpObjectLayer = mpLayers-1;
	file.close();
	return;
}

void MapManager::setTileset(const char* path) {
	mpTilesetID = sResManager->getGraphicID(path);
	mpTilesetW = sResManager->getGraphicWidth(mpTilesetID) / TILE_SIZE;
	mpTilesetH = sResManager->getGraphicHeight(mpTilesetID) / TILE_SIZE;
	return;
}

void MapManager::renderMap(C_Rectangle cam, bool over) {
	if (mpTilesetID < 0) {
		std::cout << "ERROR Tileset has not been set" << std::endl;
		system("pause");
		exit(0);
		return;
	}
	ofSetColor(255, 255, 255);
	int offsetX = cam.x, offsetY = cam.y;
	int width = cam.w + TILE_SIZE;
	int height = cam.h + TILE_SIZE;
	int beg = 0;
	int end = mpObjectLayer;
	if (over) {
		beg = mpObjectLayer;
		end = mpLayers;
	}

	int tile_begX = offsetX / 32;
	int tile_begY = offsetY / 32;
	int tile_endX = (offsetX + width)/32;
	int tile_endY = (offsetY + height)/32;

	if (tile_begX < 0) { tile_begX = 0; }
	if (tile_begY < 0) { tile_begY = 0; }
	if (tile_endX >= mpMapLayers[0][0].size()) { tile_endX = mpMapLayers[0][0].size() - 1; }
	if (tile_endY >= mpMapLayers[0].size()) { tile_endY = mpMapLayers[0].size() - 1; }

	for (int l = beg; l < end; l++) {
		for (int i = tile_begY; i <= tile_endY; i++) {
			for (int j = tile_begX; j <= tile_endX; j++) {
				int val = mpMapLayers[l][i][j];
				if (val > 1) {
					mpTilesetRect.x = (val % mpTilesetW) * TILE_SIZE;
					mpTilesetRect.y = (val / mpTilesetW) * TILE_SIZE;
					imgRender(mpTilesetID, j*TILE_SIZE - offsetX, i*TILE_SIZE - offsetY, mpTilesetRect);
				}
				//render mpMapLayers[l][i][j]
			}
		}
	}
	
}

bool MapManager::getCollision(unsigned int x, unsigned int y) {
	if (mpMapLoaded && (x >= 0 && x < mpMapWidth) && (y >= 0 && y < mpMapHeight)) {
		return mpCollisionLayer[y][x];

	}
	return true;
}