#ifndef NPC_H
#define NPC_H

#include "Entity.h"

class NPC : public Entity
{
	public:
		NPC();
		~NPC();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render();
		virtual void update();


		void updateControls();
		void move();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "NPC";};

		

		

	protected:

		int mpRandom;
		
};

#endif
