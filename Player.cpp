//Include our classes
#include "Player.h"
#include "singletons.h"


Player::Player() : Entity(){
	mpSpeed = 300;
}

Player::~Player(){
}

void Player::init(){
	Entity::init();
}
void Player::init(int x, int y){
	Entity::init(x, y);
}
void Player::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}

void Player::render() {
	Entity::render();
}

void Player::update() {
	Entity::update();
}

void Player::updateControls() {
	mpDirection = NONE;
	if (key_down['W'] || key_down['w']) {
		mpDirection = UP;
	}
	if (key_down['A'] || key_down['a']) {
		mpDirection = LEFT;
	}
	if (key_down['S'] || key_down['s']) {
		mpDirection = DOWN;
	}
	if (key_down['D'] || key_down['d']) {
		mpDirection = RIGHT;
	}
	return;
}

bool Player::isOfClass(std::string classType){
	if(classType == "Player" || 
		classType == "Entity"){
		return true;
	}
	return false;
}

int Player::getLife() {
	return mLife;
}

void Player::setLife(int life) {
	mLife = life;
}

int Player::getLoading() {
	return mLoading;
}

void Player::setAttack(bool att) {
	mAttack = att;
	return;
}

bool Player::getAttack() {
	return mAttack;
}

std::string Player::getName() {
	return mName;
}

bool Player::getMenu() {
	return mMenu;
}

void Player::setMenu(bool men) {
	mMenu = men;
	return;
}

