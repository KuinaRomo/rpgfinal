#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"

class Player : public Entity
{
	public:
		Player();
		~Player();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render();
		virtual void update();

		int getLife();
		void setLife(int life);



		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Player";};

		int getLoading();
		void setAttack(bool att);
		bool getAttack();
		std::string getName();

		bool getMenu();
		void setMenu(bool men);

	protected:
		int mLife;
		int mTotalLoad;
		float mLoading;

		bool mAttack;
		bool mMenu;

		std::string mName;
};

#endif
