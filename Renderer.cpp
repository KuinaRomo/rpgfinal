//Include our classes
#include "singletons.h"
#include "Renderer.h"
#include "ofMain.h"


void imgRender(int ID, int x, int y, C_Rectangle rect) {
	ofImage* img = sResManager->getGraphicByID(ID);
	img->drawSubsection(x, y, rect.w, rect.h, rect.x, rect.y);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, int alpha) {
	ofSetColor(255, 255, 255, alpha);
	imgRender(ID, x, y, rect);
	ofSetColor(255, 255, 255, 255);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, RenderColor color, int alpha) {
	ofSetColor(color.r, color.g, color.b, alpha);
	imgRender(ID, x, y, rect);
	ofSetColor(255, 255, 255, 255);
	return;
}

void imgRender(int ID, int x, int y, C_Rectangle rect, int r, int g, int b, int alpha) {
	ofSetColor(r, g, b, alpha);
	imgRender(ID, x, y, rect);
	ofSetColor(255, 255, 255, 255);
	return;
}