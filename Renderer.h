#ifndef RENDERER_H
#define RENDERER_H

#include "includes.h"

typedef struct {
	int r = 255;
	int g = 255;
	int b = 255;
} RenderColor;

void imgRender(int ID, int x, int y, C_Rectangle rect); 

void imgRender(int ID, int x, int y, C_Rectangle rect, int alpha);

void imgRender(int ID, int x, int y, C_Rectangle rect, RenderColor color, int alpha = 255);

void imgRender(int ID, int x, int y, C_Rectangle rect, int r, int g, int b, int alpha = 255);

#endif
