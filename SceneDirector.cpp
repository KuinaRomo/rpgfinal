//Include our classes
#include "SceneDirector.h"

//Escenas "navegables"
#include "SceneMenu.h"
#include "SceneGame.h"
#include "SceneFight.h"
//A�adir a medida que vayamos creando nuevas escenas



SceneDirector* SceneDirector::pInstance = NULL;

SceneDirector* SceneDirector::getInstance(){
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new SceneDirector();
	}
	return pInstance;
}

SceneDirector::SceneDirector(){
}

SceneDirector::~SceneDirector(){
}

void SceneDirector::init(){
	mVectorScenes.resize(SceneEnum::SCENE_LAST+1);
	mFlags.resize(SceneFlags::FLAG_LAST+1, false);

	SceneMenu* menu = new SceneMenu();
	mVectorScenes[MAIN] = menu;
	menu->init();

	SceneGame* level = new SceneGame();
	mVectorScenes[LEVEL] = level;
	level->init();

	SceneFight* fight = new SceneFight();
	mVectorScenes[FIGHT] = fight;
	fight->init();

	//A�adir e inicializar nuevas escenas aqu�

	//Primera escena del juego
	mCurrScene = MAIN;
}

void SceneDirector::changeScene(SceneEnum next_scene, bool load_on_return, bool history){
	mVectorScenes[mCurrScene]->setLoaded(false);
	mVectorScenes[mCurrScene]->setReloadOnReturn(load_on_return);

	if(history){
		mPrevScene = mCurrScene;
	}
	mCurrScene = next_scene;
}

void SceneDirector::goBack(bool load_on_return){
	mVectorScenes[mCurrScene]->setLoaded(false);
	mVectorScenes[mCurrScene]->setReloadOnReturn(load_on_return);
	mCurrScene = mPrevScene;
}

Scene* SceneDirector::getCurrentScene(){
	return mVectorScenes[mCurrScene];
}

void SceneDirector::exitGame(){
	exit(0);
}
