//Include our classes
#include "Spartan.h"

Spartan::Spartan() : PlayerFight() {
	mLife = 200;
	mTotalLoad = 250;
	mLoading = 0;
	mAttack = false;
	mMenu = false;
	mActionChosed = false;
	mPrepared = false;
	mActionSelected = "";
	mName = "Spartan";
	
	mPositionCursor = 0;
	mActions.resize(4);
	mActions[0] = "Attack";
	mActions[1] = "Magic";
	mActions[2] = "Item";
	mActions[3] = "Escape";

	mLoadVelocity = 0.8;
}

Spartan::~Spartan(){
}

void Spartan::update(){
	PlayerFight::update();
}

void Spartan::render() {
	PlayerFight::render();
}

bool Spartan::isOfClass(std::string classType){
	if(classType == "Spartan" || classType == "Player" || classType == "Entity"){
		return true;
	}
	return false;
}