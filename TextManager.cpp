//Include our classes
#include "TextManager.h"
#include "singletons.h"

TextManager* TextManager::pInstance = NULL;

TextManager* TextManager::getInstance() {
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new TextManager();
	}
	return pInstance;
}

TextManager::TextManager(){
	mTextLanguage = SPA;
	translate_path(mTextLanguage);
}

TextManager::~TextManager(){
}

void TextManager::update(){
	
}
void TextManager::render(){
	
}

bool TextManager::isOfClass(std::string classType){
	if(classType == "TextManager"){
		return true;
	}
	return false;
}

void TextManager::translate_path(Language lang) {
	std::string line, tag, sentence;
	std::string path = "assets/text/";
	size_t separateChar;
	int count = 0;
	std::fstream readText;	
	switch (lang) {
		case ENG:
			path.append("eng/texto.txt");
			break;
		case SPA:
			path.append("spa/texto.txt");
			break;
		case JAP:
			path.append("jap/texto.txt");
			break;
		case CAT:
			path.append("cat/texto.txt");
			break;
		}
	readText.open(path, std::ios::in);
	if (!readText.is_open()) {
		std::cout << "error" << std::endl;
	}
	else {
		while (!readText.eof()){
			std::getline(readText, line);
			if (line[0] != '#') {
				separateChar = line.rfind("\t");
				tag = line.substr(0, separateChar);
				sentence = line.substr(separateChar + 1);
				mTextMap.insert(std::pair<std::string, std::string>(tag, sentence));
			}	
		}
		readText.close();
	}
}

const char* TextManager::getText(std::string tag) {
	std::map<std::string, std::string>::iterator iTag = mTextMap.find(tag);
	if (iTag == mTextMap.end()) {
		return "[!]";
	}
	else {
		return iTag->second.data();
	}
}

void TextManager::changeLanguange(Language lang) {
	mTextMap.clear();
	translate_path(lang);
}