#ifndef TextManager_H
#define TextManager_H

#include "includes.h"
#include <map>

enum Language{ENG,SPA,CAT,JAP};

class TextManager
{
	public:

		//! Gets Singleton instance
		/*!
		\return Instance of TextManager (Singleton).
		*/

		static TextManager* getInstance();

		TextManager();
		~TextManager();



		void update();
		void render();

		bool isOfClass(std::string classType);
		std::string getClassName(){return "TextManager";};

		void translate_path(Language lang);

		const char* getText(std::string tag);

		void changeLanguange(Language lang);

	protected:

		static TextManager* pInstance; /*!<  Singleton instance*/

		std::map<std::string, std::string> mTextMap;
		Language mTextLanguage;
	
};

#endif
