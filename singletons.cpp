#include "singletons.h"

ResourceManager*	sResManager;
SceneDirector*		sDirector;
//GameState*			sGameState;
MapManager*			sMapManager;
AudioManager*		sAudManager;
TextManager*        sTextManager;

void instanceSingletons(){
	sResManager	  = ResourceManager::getInstance();
	sAudManager = AudioManager::getInstance();
	sDirector	  = SceneDirector::getInstance();
	//sGameState	  = GameState::getInstance();
	sMapManager   = MapManager::getInstance();
	sTextManager = TextManager::getInstance();
}