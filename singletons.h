#ifndef SINGLETONS_H
#define SINGLETONS_H

#include "ResourceManager.h"
#include "AudioManager.h"
#include "SceneDirector.h"
//#include "GameState.h"
#include "MapManager.h"
#include "TextManager.h"

extern ResourceManager*	sResManager;	/*!<  Handler for loading graphical assets*/
extern AudioManager*	sAudManager;	/*!<  Handler for loading audio assets*/
extern SceneDirector*	sDirector;		/*!<  Handler for Scenes*/
//extern GameState*		sGameState;		/*!<  Handler for game variables and save/load */
extern MapManager*		sMapManager;	/*!<  Handler for loading and displaying tilemaps*/
extern TextManager*      sTextManager;

void instanceSingletons();

#endif
